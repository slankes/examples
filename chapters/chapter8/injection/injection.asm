DEFAULT REL

SECTION .data
	message db "Hello students!", 10, 13, 0

SECTION .text

; define main as globale, which is recognizable by the linker  
global main

main:
	; print message
	mov rdi, 1 ; file descriptor
	mov rsi, message
	mov rdx, 16 ; message length
	mov rax, 1 ; syscall number for write
	syscall
	
	; exit application
	mov rdi, 0 ; error number
	mov rax, 60 ; syscall number for exit
	syscall

%ifidn __OUTPUT_FORMAT__,elf64
section .note.GNU-stack noalloc noexec nowrite progbits
%endif
