#include <stdio.h>
#include <string.h>

extern char code[];

/// Typical implementation of strcpy
/*char* strcpy(char *dest, const char *src) {
	size_t i;

	for (i = 0; src[i] != '\0'; i++)
		dest[i] = src[i];

	return dest;
}*/

void badfunc(char* arg) {
	char str[64];
	strcpy(str, arg);
}

int main(int argc, char** argv) {
	printf("Call badfunc\n");
	badfunc(code);
	printf("Leave program\n");

	return 0;
}
