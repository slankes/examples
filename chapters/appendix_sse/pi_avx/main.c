#include <stdalign.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

alignas(32) double step;
alignas(32) double sum;
alignas(32) long long num_steps = 1000000;

alignas(32) double four[] = {4.0, 4.0, 4.0, 4.0};
alignas(32) double two[] = {2.0, 2.0, 2.0, 2.0};
alignas(32) double one[] = {1.0, 1.0, 1.0, 1.0};
alignas(32) double ofs[] = {0.5, 1.5, 2.5, 3.5};

int hasAVX(void);
void calcPi_AVX(void);

int main(int argc, char **argv)
{
	struct timeval start, end;

	if (argc > 1)
		num_steps = atoll(argv[1]);
	if (num_steps < 100)
		num_steps = 1000000;
	printf("num_steps = %lld\n", num_steps);

	if (hasAVX()) {
		gettimeofday(&start, NULL);

		sum = 0.0;
		step = 1.0 / (double)num_steps;

		calcPi_AVX();

		gettimeofday(&end, NULL);
		printf("PI = %f (AVX)\n", sum * step);
		printf("Time : %lf sec (AVX)\n", (double)(end.tv_sec-start.tv_sec)+(double)(end.tv_usec-start.tv_usec)/1000000.0);
	} else {
		printf("System doesn't support AVX!\n");
	}

	return 0;
}
