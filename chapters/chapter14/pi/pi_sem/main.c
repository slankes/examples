#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>
#include <sys/sem.h>

sem_t sem;
long long num_steps = 10000000;
double step;
double sum;

typedef struct {
	long long start, end;
} thread_param;

static void *thread_func(void *arg)
{
	thread_param *thr_arg = (thread_param *) arg;

	for (long long i = thr_arg->start; i < thr_arg->end; i++) {
		double x = (i + 0.5) * step;
		sem_wait(&sem);
		sum += 4.0 / (1.0 + x * x);
		sem_post(&sem);
	}

	return 0;
}

#define MAX_THREADS	2

int main(int argc, char** argv)
{
	pthread_t threads[MAX_THREADS];
	thread_param thr_arg[MAX_THREADS];
	struct timeval start, end;

	step = 1.0 / (double)num_steps;
	sum = 0.0;
	sem_init(&sem, 0, 1);

	if (argc > 1)
		num_steps = atoll(argv[1]);
	if (num_steps < 100)
		num_steps = 1000000;
	printf("\nnum_steps = %lld\n", num_steps);

	gettimeofday(&start, NULL);

	for (int i = 0; i < MAX_THREADS; i++) {
		thr_arg[i].start = i * (num_steps / MAX_THREADS);
		thr_arg[i].end = (i + 1) * (num_steps / MAX_THREADS);

		pthread_create(&(threads[i]), NULL, thread_func, &(thr_arg[i]));
	}

	for (int i = 0; i < MAX_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	gettimeofday(&end, NULL);

	printf("Pi = %lf\n", sum * step);
	printf("Time : %lf sec\n", (double)(end.tv_sec-start.tv_sec)+(double)(end.tv_usec-start.tv_usec)/1000000.0);

	return 0;
}
